package io.reflectoring;

import io.reflectoring.api.UserApiDelegate;
import io.reflectoring.model.User;
import org.springframework.http.ResponseEntity;
import org.springframework.web.context.request.NativeWebRequest;

import java.util.Optional;

public class UserAPIDelegateImpl implements UserApiDelegate  {
    @Override
    public Optional<NativeWebRequest> getRequest() {
        return Optional.empty();
    }

    @Override
    public ResponseEntity<Void> createUser(User body) {
        return null;
    }

    @Override
    public ResponseEntity<Void> deleteUser(String username) {
        return null;
    }

    @Override
    public ResponseEntity<User> getUserByName(String username) {
        return null;
    }

    @Override
    public ResponseEntity<User> updateUser(String username, User body) {
        return null;
    }
}
